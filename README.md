# sisop-praktikum-modul-4-2023-WS-F04
# Praktikum Sistem Operasi Modul 4
# Kelompok F04
- Fathan Abi Karami 5025211156
- Heru Dwi Kurniawan 5025211055
- Alya Putri Salma 5025211174

## 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

```
//untuk mendonlod data
char *command = "kaggle datasets download -d bryanb/fifa-player-stats-database";

 system(command);
 //untuk unzip file
 system ( "unzip fifa-player-stats-database.zip");
 ```

Maka output program tersebut adalah
![WhatsApp_Image_2023-06-01_at_05.20.17](/uploads/ae9a5cc4427e4727e8a3ae251c8cdde8/WhatsApp_Image_2023-06-01_at_05.20.17.jpeg)

b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

```
 // Fungsi membuka file dataset
    FILE* file = fopen("FIFA23_official_data.csv", "r"); 
    // Jika file tidak ditemukan
    if (file == NULL) { 
    	 // Mencetak pesan error
        printf("File not found.\n"); 
        // Program Berhenti
        return 1; 
    }
    
    
    // char array berfungsi menyimpan baris yang dibaca
    char line[1024]; 
    // FUngsi untuk membaca baris pertama
    if (fgets(line, sizeof(line), file)) { 
    // Fungsi Untuk Mencetak header
        printf("Name\tClub\tAge\tPotential\tPhoto\t...\n"); 
    }
    

    // Fungsi untuk membaca baris demi baris
    while (fgets(line, sizeof(line), file)) { 
        char* id = strtok(line, ",");
        char* name = strtok(NULL, ",");
        char* age = strtok(NULL, ",");
        char* photo = strtok(NULL, ",");
        char* nationality = strtok(NULL, ",");
        char* flag = strtok(NULL, ",");
        char* overall = strtok(NULL, ",");
        char* potential = strtok(NULL, ",");
        char* club = strtok(NULL, ",");
        char* clubLogo = strtok(NULL, ",");
        char* value = strtok(NULL, ",");
        char* wage = strtok(NULL, ",");
        char* special = strtok(NULL, ",");
        char* preferredFoot = strtok(NULL, ",");
        char* internationalReputation = strtok(NULL, ",");
        char* weakFoot = strtok(NULL, ",");
        char* skillMoves = strtok(NULL, ",");
        char* workRate = strtok(NULL, ",");
        char* bodyType = strtok(NULL, ",");
        char* realFace = strtok(NULL, ",");
        char* position = strtok(NULL, ",");
        char* joined = strtok(NULL, ",");
        char* loanedFrom = strtok(NULL, ",");
        char* contractValidUntil = strtok(NULL, ",");
        char* height = strtok(NULL, ",");
        char* weight = strtok(NULL, ",");
        char* releaseClause = strtok(NULL, ",");
        char* kitNumber = strtok(NULL, ",");
        char* bestOverallRating = strtok(NULL, ",");


        // Fungsi Untuk memeriksa kriteria
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) { 
        // FUngsi untuk mencetak data pemain (name, club, age, potential, photo)
            printf("%s\t%s\t%s\t%s\t%s\t...\n", name, club, age, potential, photo); 
        }
    }
    
    
    // FUngsi menutup file dataset
    fclose(file);
    return 0;
}
```
Maka program tersebut mengeluarkan output
![WhatsApp_Image_2023-06-01_at_05.57.49](/uploads/159c0a97917f769ca866cdb4fc79a08d/WhatsApp_Image_2023-06-01_at_05.57.49.jpeg)



c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

```
//Menggunakan base image terbaru (latest) dari Ubuntu, kita dapat membangun Docker Image.

# Base image
FROM ubuntu:latest

//Dalam konteks Docker, kita dapat menggunakan perintah RUN untuk melaksanakan serangkaian tindakan. Pada tahap ini, kami akan memperbarui (update) sistem menggunakan apt-get dan menginstal beberapa dependensi yang diperlukan untuk aplikasi, termasuk build-essential, wget, unzip, dan python3-pip
# Install dependencies
RUN apt-get update && \
    apt-get install -y \
    build-essential \
    wget \
    unzip \
    python3-pip

// Dalam container, kita akan menetapkan direktori kerja (working directory) menjadi /app. Semua perintah yang dieksekusi setelah ini akan dilakukan di dalam direktori tersebut.
# Set working directory
WORKDIR /app

// Untuk melakukan autentikasi API Kaggle di dalam container, kita perlu menyalin file kaggle.json dari host ke direktori /root/.kaggle/kaggle.json di dalam container.
# Copy kaggle.json to the container
COPY kaggle.json /root/.kaggle/kaggle.json

// Dalam container, kita dapat menggunakan perintah RUN untuk menginstal Kaggle CLI (Command Line Interface) menggunakan pip3.
# Install Kaggle CLI
RUN pip3 install kaggle

// Dalam container, kita dapat menggunakan perintah RUN untuk mengunduh dan mengekstrak dataset "fifa-player-stats-database" menggunakan Kaggle CLI dari Kaggle.
# Download and extract the dataset
RUN kaggle datasets download -d bryanb/fifa-player-stats-database && \
    unzip fifa-player-stats-database.zip

// Untuk menyalin file storage.c dari host ke dalam container, kita dapat menggunakan perintah COPY dan menempatkannya di direktori kerja saat ini (/app).
# Copy the source code to the container
COPY storage.c .

//Dalam container, kita dapat menggunakan perintah RUN untuk mengkompilasi file storage.c menjadi sebuah binary bernama storage menggunakan kompiler C (gcc).
# Compile the C code
RUN gcc -o storage storage.c

// Ketika container dijalankan, kita dapat mendefinisikan perintah yang akan dieksekusi. Dalam hal ini, kita akan menjalankan aplikasi dengan menjalankan binary storage yang telah dikompilasi sebelumnya.
# Run the application
CMD ["./storage"]

```
Maka Output program tersebut adalah
![Screenshot_from_2023-05-31_20-45-11](/uploads/f5aada6884c391f3ba788b9d28405d99/Screenshot_from_2023-05-31_20-45-11.png)

 d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

```
https://hub.docker.com/r/awan1016/storage-app
```
![WhatsApp_Image_2023-06-01_at_06.16.44](/uploads/7937829c3d8bc284883ceab5f4f36e3d/WhatsApp_Image_2023-06-01_at_06.16.44.jpeg)

e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

- docker-compose.yml pada Folder Barcelona:
```
version: '3'
services:
  storage-app-1:
    image: awan1016/storage-app
    ports:
      - 8001:8000

  storage-app-2:
    image: awan1016/storage-app
    ports:
      - 8002:8000

  storage-app-3:
    image: awan1016/storage-app
    ports:
      - 8003:8000

  storage-app-4:
    image: awan1016/storage-app
    ports:
      - 8004:8000

  storage-app-5:
    image: awan1016/storage-app
    ports:
      - 8005:8000
```

docker-compose.yml Pada Folder NApoli:
```
version: '3'
services:
  storage-app-1:
    image: awan1016/storage-app
    ports:
      - 9001:8000

  storage-app-2:
    image: awan1016/storage-app
    ports:
      - 9002:8000

  storage-app-3:
    image: awan1016/storage-app
    ports:
      - 9003:8000

  storage-app-4:
    image: awan1016/storage-app
    ports:
      - 9004:8000

  storage-app-5:
    image: awan1016/storage-app
    ports:
      - 9005:8000
```
Maka kedua program tersebut outputnya adalah :

- PAda Folder Barcelona
```
herukurniawan@herukurniawan-VirtualBox:~/praktikkum4/Barcelona$ sudo docker-compose up -d
Creating barcelona_storage-app-5_1 ... done
Creating barcelona_storage-app-3_1 ... done
Creating barcelona_storage-app-2_1 ... done
Creating barcelona_storage-app-1_1 ... done
Creating barcelona_storage-app-4_1 ... done
```
- Pada Folder NApoli
```
herukurniawan@herukurniawan-VirtualBox:~/praktikkum4/Napoli$ sudo docker-compose up -d
Creating network "napoli_default" with the default driver
Creating napoli_storage-app-3_1 ... done
Creating napoli_storage-app-4_1 ... done
Creating napoli_storage-app-2_1 ... done
Creating napoli_storage-app-5_1 ... done
Creating napoli_storage-app-1_1 ... done

```
![WhatsApp_Image_2023-06-01_at_06.01.06](/uploads/acd90b9ef81d23a6e9e3dede46fba9f9/WhatsApp_Image_2023-06-01_at_06.01.06.jpeg)

# Soal 2

Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:

## Poin A
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

## Solusi 
Langkah pertama kita harus membuat sebuah aplikasi fuse
```bash
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
}
```

Kemudian membuat fungsi pada xmp_getattr, xmp_readdir, dan xmp_read sebagai berikut.

Membuat Fungsi `xmp_getattr:` yang digunakan untuk mendapatkan atribut dari sebuah file atau direktori.
```bash
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

```
Fungsi `xmp_read:` ini digunakan untuk membaca isi dari sebuah file.
```bash
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

```
Fungsi `xmp_mkdir:` ini digunakan untuk membuat direktori baru. Fungsi ini menggabungkan path dari direktori utama dengan path direktori baru yang diminta, kemudian memanggil fungsi mkdir untuk membuat direktori tersebut.
```bash
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[10000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);


    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL)
    {
        sprintf(desc, "Create directory %s", fpath);
        logged("FAILED", "MKDIR", desc);
        return -1;
    }

    res = mkdir(fpath, mode);

    sprintf(desc, "Create directory %s", fpath);
    logged("SUCCESS", "MKDIR", desc);

    if (res == -1) return -errno;
    return 0;

}

```

## Poin B dan C
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/

Kemudian, buat folder projectMagang di dalamnya.
Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

## Solusi
Membuat fungsi rename dari fuse. Bertujuan untuk mengubah nama sebuah file atau directory. Berikut adalah fungsi dari rename.

Fungsi `xmp_rename:` ini digunakan untuk mengubah nama sebuah file atau direktori. Fungsi ini menggabungkan path dari direktori utama dengan path file/direktori yang akan diubah namanya, kemudian memanggil fungsi rename untuk mengubah nama file/direktori tersebut.
```bash
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];


    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(ffrom, "%s", from);
    }
    else sprintf(ffrom, "%s%s", dirpath, from);

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fto, "%s", to);
    }
    else sprintf(fto, "%s%s", dirpath, to);

    int flag = 0;

    if (strstr(ffrom, "bypass") != NULL || strstr(ffrom, "sihir") != NULL)
    {
        flag =  1;
    }

    if (strstr(ffrom, "restricted") != NULL){
        flag = 0;
    }

    if (strstr(fto, "bypass") != NULL || strstr(fto, "sihir") != NULL)
    {
        flag =  1;
    }
    if (strstr(ffrom, "bypass") == NULL && strstr(ffrom, "sihir") == NULL && strstr(ffrom, "restricted") == NULL){
        flag = 1;
    }

    char *desc = malloc(9000);
    if (flag == 0){
        sprintf(desc, "Rename from %s to %s", ffrom, fto);
        logged("FAILED", "RENAME", desc);
        return -1;
    }

    sprintf(desc, "Rename from %s to %s", ffrom, fto);
    res = rename(ffrom, fto);

    if (res == -1) return -errno;
    logged("SUCCESS", "RENAME", desc);

    return 0;
}

```

## Poin D
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

## Solusi
Membutuhkan fungsi logged yang telah dibuat di poin A dan B yang bertujuan untuk membuat log. Berikut adalah fungsi dari logged.
```bash
void logged(char *level, char *cmd, char *desc)
{
    FILE *fp;
   
    fp = fopen("/home/alea/SISOP/modul4/logmucatatsini.log", "a+");
    
    if (fp == NULL){
        printf("[Error] : [Gagal dalam membuka file]");
        exit(1);
    }
    char *user = getUserName();
    char *description = malloc(1000 * sizeof(char));
    sprintf(description, "%s-%s", user, desc);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);
    fclose(fp);
}

```

## Kendala
Masih bingung mengenai materi Fuse

# Soal 3
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

## Poin A
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

```c
// source path
static
const char * dirpath = "/home/fathan/inifolderetc/sisop";


// FUSE necessart operator

//  Inisisasi
static void * xmp_init(struct fuse_conn_info * conn) {
   (void) conn;
   char fpath[1000];

   const char * path = "/";
   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   encode(fpath, 0);

   format(fpath);

   return NULL;
}


// get atribut
static int xmp_getattr(const char * path, struct stat * stbuf) {
   int res;
   char fpath[1000];

   sprintf(fpath, "%s%s", dirpath, path);

   res = lstat(fpath, stbuf);

   if (res == -1) return -errno;

   return 0;
}


// baca direktori
static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {

   // printf("call readdir(): %s\n", path);

   char fpath[1000];

   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   int res = 0;

   DIR * dp;
   struct dirent * de;
   (void) offset;
   (void) fi;

   dp = opendir(fpath);

   if (dp == NULL) return -errno;

   while ((de = readdir(dp)) != NULL) {
      struct stat st;

      memset( & st, 0, sizeof(st));

      st.st_ino = de -> d_ino;
      st.st_mode = de -> d_type << 12;
      res = (filler(buf, de -> d_name, & st, 0));

      if (res != 0) break;
   }

   closedir(dp);

   return 0;
}


// baca file
static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
   char fpath[1000];
   if (strcmp(path, "/") == 0) {
      path = dirpath;

      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   int res = 0;
   int fd = 0;

   (void) fi;

   fd = open(fpath, O_RDONLY);

   if (fd == -1) return -errno;

   res = pread(fd, buf, size, offset);

   if (res == -1) res = -errno;

   close(fd);

   return res;
}


// struct operator
static struct fuse_operations xmp_oper = {
   .init = xmp_init,
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,

};


// main 
int main(int argc, char * argv[]) {
   umask(0);
   
   // fuse main
   return fuse_main(argc, argv, & xmp_oper, NULL);

}
```

penjelasan:

menggunakan template seperti pada materi modul 4 dengan menambahkan operator:

* xmp_init : untuk inisisasi FUSE (menerapkan encoding pada poin B dan format penamaan pada poin c)

## poin B 
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

```c
// fungsi encode file
void encodeFile(const char * filePath) {
   FILE * file = fopen(filePath, "rb");
   if (!file) {
      fprintf(stderr, "Gagal membuka file: %s\n", filePath);
      return;
   }

   // hitung ukuran file
   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);

   // Alokasikan memory untuk isi file
   unsigned char * fileContent = malloc(fileSize);
   if (!fileContent) {
      fprintf(stderr, "Gagal mengalokasikan memori.\n");
      fclose(file);
      return;
   }

   // baca isi file
   if (fread(fileContent, 1, fileSize, file) != fileSize) {
      fprintf(stderr, "Gagal membaca file: %s\n", filePath);
      fclose(file);
      free(fileContent);
      return;
   }

   // hitung ukuran output base64
   long outputSize = 4 * ((fileSize + 2) / 3);

   // alokasikan memori untuk output base64
   char * base64Output = malloc(outputSize + 1);
   if (!base64Output) {
      fprintf(stderr, "Gagal Mengalokasikan memori.\n");
      fclose(file);
      free(fileContent);
      return;
   }

   // Encode file ke base64
   int i, j;
   for (i = 0, j = 0; i < fileSize; i += 3, j += 4) {
      unsigned char byte1 = fileContent[i];
      unsigned char byte2 = (i + 1 < fileSize) ? fileContent[i + 1] : 0;
      unsigned char byte3 = (i + 2 < fileSize) ? fileContent[i + 2] : 0;

      unsigned char index1 = byte1 >> 2;
      unsigned char index2 = ((byte1 & 0x03) << 4) | (byte2 >> 4);
      unsigned char index3 = ((byte2 & 0x0F) << 2) | (byte3 >> 6);
      unsigned char index4 = byte3 & 0x3F;

      base64Output[j] = base64Chars[index1];
      base64Output[j + 1] = base64Chars[index2];
      base64Output[j + 2] = (i + 1 < fileSize) ? base64Chars[index3] : '=';
      base64Output[j + 3] = (i + 2 < fileSize) ? base64Chars[index4] : '=';
   }
   base64Output[j] = '\0';

   // tutup file
   fclose(file);

   // write encoded base64 ke temporary file
   FILE * tempFile = fopen("temp.txt", "wb");
   if (!tempFile) {
      fprintf(stderr, "Gagal membuat temporary file.\n");
      free(fileContent);
      free(base64Output);
      return;
   }

   if (fwrite(base64Output, 1, strlen(base64Output), tempFile) != strlen(base64Output)) {
      fprintf(stderr, "Gagal melakukan write ke temporary file\n");
   }

   // tutup temporary file
   fclose(tempFile);

   // hapus file original
   if (remove(filePath) != 0) {
      fprintf(stderr, "Gagal menghapus original file: %s\n", filePath);
      free(fileContent);
      free(base64Output);
      return;
   }

   // ganti nama temporary file ke nama file original
   if (rename("temp.txt", filePath) != 0) {
      fprintf(stderr, "Gagal menngati nama temporary file.\n");
   } else {
      // printf("File berhasil di encoded\n");
   }

   // bersihkan resource
   free(fileContent);
   free(base64Output);
}

// fungsi encode direktori (berlaku secara rekursif)
void encode(const char * path, int flag) {
   DIR * dir;
   struct dirent * entry;
   struct stat fileStat;

   // buka direktory
   dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Direktori gagal untuk dibuka: %s\n", path);
      return;
   }

   // baca entri direktori
   while ((entry = readdir(dir)) != NULL) {

      // abaikan direktori sekrang dan direktori parent
      if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
         continue;
      }

      // buat full path untuk entri
      char fullpath[1024];
      snprintf(fullpath, sizeof(fullpath), "%s/%s", path, entry -> d_name);

      // dapatkan info entri
      if (stat(fullpath, & fileStat) < 0) {
         fprintf(stderr, "Failed to get file stat: %s\n", fullpath);
         continue;
      }

      if (S_ISDIR(fileStat.st_mode)) {
         // Entry adalah direktori
         // printf("Directory: %s\n", fullpath);

         // int flag = 0;

         char firstLetter = entry -> d_name[0];
         if (flag == 1 || firstLetter == 'l' || firstLetter == 'u' || firstLetter == 't' || firstLetter == 'h' || firstLetter == 'L' || firstLetter == 'U' || firstLetter == 'T' || firstLetter == 'H') {
            encode(fullpath, 1);
         } else {
            encode(fullpath, 0);
         }

         //encode direktorii secara rekursif

      } else if (S_ISREG(fileStat.st_mode)) {
         // entry adalah file biasa

         if (flag == 1) {
            // encode
            encodeFile(fullpath);
            // printf("File: %d %s\n", flag, fullpath);

         }
         //printf("File: %d %s\n", flag, fullpath);
      }
   }

   // tutup direktori
   closedir(dir);
}
```

Penjelasan encode():

1. Fungsi encode() akan dipanggil pada saat FUSE diinisiasi (xmp_init)
2. kemudian akan membuka direktori dan membaca entrynya.
3. jika entri adalah berupa direktori maka cek apakah direktori diawali dengan huruf ("L", "U", "T", atau "H") atau telah diberi flag, jika ya maka encode() direktori dengan flag = 1 dan path adalah path entri direktori, lalu kembali ke langkah 2. jika tidak maka  lakuka rekursi encode() direktori dengan flag = 0 dan path adalah path entri direktori. kembali ke langkah 2
4. jika entri berupa file biasa, cek apakah flag=1, jika ya encodeFile(). 
5. lakukan pada semua entri direktori

## Poin C
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

```c
void format(const char * path) {
   DIR * dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Gagal membuka direktori: %s\n", path);
      return;
   }

   struct dirent * entry;
   while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) {
         // buat full path
         char oldFilePath[1024];
         snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", path, entry -> d_name);

         // buat nama file dengan lowercase
         char newFileName[1024];
         strcpy(newFileName, entry -> d_name);
         int len = strlen(newFileName);
         for (int i = 0; i < len; i++) {
            newFileName[i] = tolower(newFileName[i]);
         }

         // buat full path file baru
         char newFilePath[1030];
         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);

         len = strlen(newFileName);
         if (len <= 4) {
            char newName[256] = "";
            int div;
            size_t index = 0;

            for (int i = 0; i < len; i++) {
               div = 128; // 8th bit
               while (div) {
                  newName[index] = ((newFileName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }
            }

            strcpy(newFileName, newName);
         }

         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);

         // Ganti nama file
         if (rename(oldFilePath, newFilePath) != 0) {
            fprintf(stderr, "Gagal mengganti nama file: %s\n", oldFilePath);
            continue;
         } else {
            // printf("sukses mengganti nama file: %s\n", newFilePath);
         }

      } else if (entry -> d_type == DT_DIR && strcmp(entry -> d_name, ".") != 0 && strcmp(entry -> d_name, "..") != 0) {
         // buat full path direktoru
         char oldDirPath[1024];
         snprintf(oldDirPath, sizeof(oldDirPath), "%s/%s", path, entry -> d_name);

         // buat nama direktori baru dengan uppercase semua
         char newDirName[1024];
         strcpy(newDirName, entry -> d_name);
         int len = strlen(newDirName);
         for (int i = 0; i < len; i++) {
            newDirName[i] = toupper(newDirName[i]);
         }

         // buat full path direktori baru
         char newDirPath[1500];
         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);

         len = strlen(newDirName);
         if (len <= 4) {

            char newName[256] = "";
            int div;
            size_t index = 0;

            for (int i = 0; i < len; i++) {

               div = 128; // 8th bit
               while (div != 0) {
                  newName[index] = ((newDirName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len - 1) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }

            }

            strcpy(newDirName, newName);
         }

         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);

         // Ganti nama direktori
         if (rename(oldDirPath, newDirPath) != 0) {
            fprintf(stderr, "Gagal mengganti nama direktori: %s\n", oldDirPath);
            continue;
         } else {
            // printf("sukses mengganti nama direktori: %s\n", newDirPath);
         }

         // pannggil function secara rekursif

         format(newDirPath);
      }

   }

   // Close the directory
   closedir(dir);
}
```

penjelasan forma():
1. Buka direktori
2. baca entri direktori
3. jika entri adalah file maka ubah nama file menjadi lower case semua. kemudian cek apakah jumlah huruf nama file <= 4, jika ya maka ubah ke format binary yang didapat dari ASCII code masing-masing karakter.
4. jika entri adalah direktori maka ubah nama direktori menjadi upper case semua. kemudian cek apakah jumlah huruf nama direktori <= 4. jika ya maka ubah ke format binary yang didapat dari ASCII code masing-masing karakter. lakukan rekursi format()dengan argumen path adalah path entri direktori

## Poin D
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah)

## Poin E
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
* Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
* Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 

## Poin F
Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂


## Output
terminal:
![](./img/soal3/terminal.png)
SS FUSE folder:
![](./img/soal3/fuse_folder.png)
SS contoh encoded file:
![](./img/soal3/encoded_file_example_1.png)
![](./img/soal3/encoded_file_example_2.png)
SS


## Kendala dan Revisi
1. Poin B (Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat) ✅

2. Poin C (Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.) ✅

3. Poin D
4. Poin E
5. Poin F




