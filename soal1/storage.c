#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main() {

//untuk mendonlod data
char *command = "kaggle datasets download -d bryanb/fifa-player-stats-database";

 system(command);
 //untuk unzip file
 system ( "unzip fifa-player-stats-database.zip");
 
    // Fungsi membuka file dataset
    FILE* file = fopen("FIFA23_official_data.csv", "r"); 
    // Jika file tidak ditemukan
    if (file == NULL) { 
    	 // Mencetak pesan error
        printf("File not found.\n"); 
        // Program Berhenti
        return 1; 
    }
    
    
    // char array berfungsi menyimpan baris yang dibaca
    char line[1024]; 
    // FUngsi untuk membaca baris pertama
    if (fgets(line, sizeof(line), file)) { 
    // Fungsi Untuk Mencetak header
        printf("Name\tClub\tAge\tPotential\tPhoto\t...\n"); 
    }
    

    // Fungsi untuk membaca baris demi baris
    while (fgets(line, sizeof(line), file)) { 
        char* id = strtok(line, ",");
        char* name = strtok(NULL, ",");
        char* age = strtok(NULL, ",");
        char* photo = strtok(NULL, ",");
        char* nationality = strtok(NULL, ",");
        char* flag = strtok(NULL, ",");
        char* overall = strtok(NULL, ",");
        char* potential = strtok(NULL, ",");
        char* club = strtok(NULL, ",");
        char* clubLogo = strtok(NULL, ",");
        char* value = strtok(NULL, ",");
        char* wage = strtok(NULL, ",");
        char* special = strtok(NULL, ",");
        char* preferredFoot = strtok(NULL, ",");
        char* internationalReputation = strtok(NULL, ",");
        char* weakFoot = strtok(NULL, ",");
        char* skillMoves = strtok(NULL, ",");
        char* workRate = strtok(NULL, ",");
        char* bodyType = strtok(NULL, ",");
        char* realFace = strtok(NULL, ",");
        char* position = strtok(NULL, ",");
        char* joined = strtok(NULL, ",");
        char* loanedFrom = strtok(NULL, ",");
        char* contractValidUntil = strtok(NULL, ",");
        char* height = strtok(NULL, ",");
        char* weight = strtok(NULL, ",");
        char* releaseClause = strtok(NULL, ",");
        char* kitNumber = strtok(NULL, ",");
        char* bestOverallRating = strtok(NULL, ",");


        // Fungsi Untuk memeriksa kriteria
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) { 
        // FUngsi untuk mencetak data pemain (name, club, age, potential, photo)
            printf("%s\t%s\t%s\t%s\t%s\t...\n", name, club, age, potential, photo); 
        }
    }
    
    
    // FUngsi menutup file dataset
    fclose(file);
    return 0;
}

